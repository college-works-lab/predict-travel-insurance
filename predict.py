#from sklearn.externals import joblib
import joblib
import numpy as np
import pandas as pd

dataset = pd.read_csv('csv_export.csv')

filename = 'model.sav'
loaded_model = joblib.load(filename)

result = loaded_model.predict(dataset)
print(result[0])