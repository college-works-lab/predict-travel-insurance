<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Predict Travel Insurance</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin - v2.4.1
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->

  <?php
/* 
  function export_csv($data) {
      
    $header_args = array( "Employment Type", "GraduateOrNot", "AnnualIncome" , "FamilyMembers", "ChronicDiseases", "FrequentFlyer", "EverTravelledAbroad");
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=csv_export.csv');
    $output = fopen( 'php://output', 'w' );
    ob_end_clean();
    fputcsv($output, $header_args);
    
    foreach($data AS $data_item){
        fputcsv($output, $data_item);
    }
    
    fclose($output);
  }


  $notif_form = null;
  $notif_predict = "";
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!empty($_POST)) {

      // income family disease flyer travel
      $employ = $_POST['employment'];
      $graduate = $_POST['graduate'];
      $income = $_POST['income'];
      $family = $_POST['family'];
      $disease = $_POST['disease'];
      $flyer = $_POST['flyer'];
      $travel = $_POST['travel'];



      $data = array(
        '0' => array( $employ, $graduate, $income, $family, $disease, $flyer, $travel ),
        );

      export_csv($data);

      $notif_form = "<div class=\"alert alert-danger alert-dismissable\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button><strong>Error!</strong> Please fill all the required fields.</div";
    }
  } */
  
  ?>
</head>

<body class="toggle-sidebar">
<!-- style="margin-top:0px;" -->
  <main id="main" class="main">

    <div class="pagetitle">
      <h1>Predict Travel Insurance</h1>
    </div><!-- End Page Title -->

    <section class="section dashboard">
      <div class="row">

      <?php
      if (isset($_GET["prediction"])) {
        ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <i class="bi bi-check-circle me-1"></i>
            Prediction is completed successfully
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
          </div>
        <?php
      }
      ?>
          


        <!-- Right side columns -->
        <div class="col-lg-12">

 <!-- Sales Card -->
              <div class="card info-card sales-card">

                <div class="card-body">
                  <div class="card-title row">
                    <h5 class=" col">Claim Travel Insurance? <span>| click predict</span></h5>
                    <div class="text-end col-5">
                      <form action="process.php" method="GET">
                      <button type="submit" class="btn btn-primary">Predict</button>
                      <input type="hidden" value="1" name="predict">
                      </form>
                    </div>
                  </div>

                  <?php 
                  if (isset($_GET["prediction"])){
                    ?>

                    <div class="d-flex align-items-center">
                    <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                      <i class="bi bi-cart"></i>
                    </div>
                    <div class="ps-3">
                      <h6><?= $_GET["prediction"] == '1' ? 'Yes' : 'No' ?></h6>
                      <!-- <span class="text-success small pt-1 fw-bold">8%</span> <span class="text-muted small pt-2 ps-1">increase</span> -->
                    </div>
                  </div>
                    
                    <?php
                  }
                  ?>
                </div>

              </div>
            <!-- End Sales Card -->

        </div><!-- End Right side columns -->


        <!-- Left side columns -->
        <div class="col-lg-12">
          <div class="row">
            <!-- Form -->
            <div class="col-12">
            <?php /* echo htmlspecialchars($_SERVER["PHP_SELF"]); */?>
              <form class="card top-selling overflow-auto" action="build_csv.php" method="POST">

                <div class="card-body pb-0">
                  <div class="card-title row">
                  <h5 class=" col">Input Your Data <span>| Here</span></h5>
                  <div class="text-end col-5">
                      <button type="reset" class="btn btn-secondary">Reset</button>
                      <button type="submit" class="btn btn-primary">Submit & Download</button>
                    </div>
                  </div>
                  
                  <!-- Vertical Form -->
                  <div class="row g-3 mb-4" >
                  
                    <div class="col-12">
                      <label for="employment_type" class="form-label">Employment Type</label>
                      <select id="employment_type" class="form-select" name="employment">
                        <option value="0" selected>Government Sector</option>
                        <option value="1" >Private Sector/Self Employed</option>
                      </select>
                    </div>
                    <div class="col-12">
                      <label for="graduate" class="form-label">Graduate or Not</label>
                      <select id="graduate" class="form-select" name="graduate">
                        <option value="0" selected>No</option>
                        <option value="1" >Yes</option>
                      </select>
                    </div>
                    <div class="col-12">
                      <label for="income" class="form-label">Annual Income</label>
                      <select id="income" class="form-select" name="income">
                        <option value="0" selected>< 75,000,000</option>
                        <option value="1" >75,000,000 - 125,000,000</option>
                        <option value="2" >125,000,000 - 175,000,000</option>
                        <option value="3" >175,000,000 - 225,000,000</option>
                        <option value="4" >225,000,000 - 275,000,000</option>
                        <option value="5" >>= 275,000,000</option>
                      </select>
                    </div>
                    <div class="col-12">
                      <label for="family" class="form-label">Family Members</label>
                      <input type="text" class="form-control" id="family" name="family" value="0">
                    </div>
                    <div class="col-12">
                      <label for="disease" class="form-label">Chronic Diseases</label>
                      <select id="disease" class="form-select" name="disease">
                        <option value="0" selected>No</option>
                        <option value="1" >Yes</option>
                      </select>
                    </div>
                    <div class="col-12">
                      <label for="flyer" class="form-label">Frequent Flyer</label>
                      <select id="flyer" class="form-select" name="flyer">
                        <option value="0" selected>No</option>
                        <option value="1" >Yes</option>
                      </select>
                    </div>
                    <div class="col-12">
                      <label for="travel" class="form-label">Ever Travelled Abroad</label>
                      <select id="travel" class="form-select" name="travel">
                        <option value="0" selected>No</option>
                        <option value="1" >Yes</option>
                      </select>
                    </div>
                    
                  </div>
                  <!-- Vertical Form -->
                </div>

              </form>
            </div><!-- End Top Selling -->

          </div>
        </div><!-- End Left side columns -->

      </div>
    </section>

  </main><!-- End #main -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>