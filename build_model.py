# py -m pip install scikit-learn
# installed threadpoolctl, scipy, joblib, scikit-learn
# pandas = pytz, dateutil
import numpy as np
import pandas as pd
#import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
import csv
from sklearn import tree

ds = pd.read_csv('TravelInsurancePrediction_ExcelClean.csv')

from sklearn.preprocessing import LabelEncoder
# Change string or categorical data into integer
# encode data type
encoder = LabelEncoder()

for col in ds.columns:
  if ds[col].dtypes == 'object':         
    #encoder = LabelEncoder()         
    ds[col] = encoder.fit_transform(ds[col])

x=ds.iloc[:,1:-1]
y=ds.iloc[:,-1]

xTrain,xTest,yTrain,yTest = train_test_split(x,y, test_size=0.3) # split data test and train


modelf=RandomForestClassifier(n_estimators=555, random_state=15)
modelf.fit(xTrain,yTrain)

yPredf=modelf.predict(xTrain)
yPred1f=modelf.predict(xTest)


#from sklearn.externals import joblib
import joblib


#save model
filename = 'model.sav'
joblib.dump(modelf, filename)
#load model
loaded_model = joblib.load(filename)
